#!/bin/sh

# patch-nvim.sh
# A quick script for copying over my system's nvim config to the repository.

# Don't forget to call it from within the git repository's root!
# $ ./testing/patch-nvim.sh
# NOT:
# $ cd testing
# ./patch-nvim.sh

error() {
	echo "$1" 1>&2
	exit 1
}

# Checks if a directory exists and displays an optional text message
dir_check() {
	search=$1
	if [ "$#" -eq "2" ]; then
		[ ! -d "$1" ] && error "$2"
	elif [ "$#" -eq "1" ]; then 
		[ ! -d "$1" ] && error "Could not find \"$1\""
	else
		error "Invalid arguments to dir_check()"
	fi
}

dir_check "${XDG_CONFIG_HOME:-$HOME/.config}/nvim"
dir_check "./.git" "Not inside a git root. Please run this script from the repository's root."
dir_check "./.config/nvim"
rm -rf "./.config/nvim"
cp -r "${XDG_CONFIG_HOME:-$HOME/.config}/nvim" "./.config"
rm -rf "./.config/nvim/plugin"
